package testingFramework;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AddDeal extends TestHelper {
	
	public static String defaultPerson = "Aleksei Suvorov";
	public static String defaultCompany = "Cabana Parrot O�";
	public static String startUrl = ("https://cabanaparrotoue.pipedrive.com/pipeline");

	@Test
	public static void CreateWithNewUser() throws InterruptedException {
		
		driver.get(startUrl);
		
		Thread.sleep(1500);
		driver.findElement(By.id("pipelineAddDeal")).click();
		
		Thread.sleep(1000);
		String nextUser = String.valueOf(System.currentTimeMillis() / 1000L);
		driver.findElement(By.name("person")).sendKeys("user_" + nextUser);
		
		driver.findElement(By.name("expected_close_date")).sendKeys("01.04.2018");
		
		driver.findElement(By.name("value")).click();
		driver.findElement(By.name("value")).sendKeys("10");
		
		driver.findElement(By.xpath("//span[text()='Entire company']")).click();
		
		Thread.sleep(500);
		driver.findElement(By.xpath("//h5[text()='Owner & followers']")).click();
		
		driver.findElement(By.xpath("//span[text()='Save']")).click();
		
		// verify that the deal was created - navigate to it
		Thread.sleep(1500);
		driver.findElement(By.xpath("//strong[text()='user_"+nextUser+" deal']")).click();
		
		// verify the correct due date (past date - overdue)
		Thread.sleep(1500);
		String dueDate = driver.findElement(By.cssSelector("div[class='expectedCloseDateWrapper overdue']")).getText();
		Assert.assertEquals(dueDate, "1. aprill 2018");
		
		// verify the correct deal value (default currency)
		String dealValue = driver.findElement(By.cssSelector("div[class='value']")).getText();
		Assert.assertEquals(dealValue, "10 �");
		
		// check for correctly set permissions (non-default permissions were set)
		driver.findElement(By.cssSelector("div[class='valueWrap clearfix visible_to permissionsSecondRegular']")).click();
		Thread.sleep(500);
		String ownerText = "Owner & followers";
		String permissionValue = driver.findElement(By.xpath("//li[@class='active']/h5")).getText();
		Assert.assertEquals(permissionValue, ownerText);
		
	}
	
	@Test
	private static void CreateWithExistingUser() throws InterruptedException {
		
		driver.get(startUrl);
		
		Thread.sleep(1500);
		driver.findElement(By.id("pipelineAddDeal")).click();
		
		Thread.sleep(500);
		driver.findElement(By.name("expected_close_date")).sendKeys("02.02.2020");		
		driver.findElement(By.name("value")).click();
		driver.findElement(By.name("value")).sendKeys("22");
		driver.findElement(By.cssSelector("div[class='select2-container currency']")).click();
		Thread.sleep(1000);
		driver.findElement(By.cssSelector("input[class='select2-input select2-focused']")).sendKeys("USD");
		Thread.sleep(1000);
		driver.findElement(By.cssSelector("input[class='select2-input select2-focused']")).sendKeys(Keys.RETURN);
		
		Thread.sleep(500);
		driver.findElement(By.name("person")).sendKeys("aleksei");
		
		Thread.sleep(1000);
		driver.findElement(By.name("person")).sendKeys(Keys.DOWN);
		driver.findElement(By.name("person")).sendKeys(Keys.RETURN);
		
		Thread.sleep(500);
		driver.findElement(By.xpath("//span[text()='Save']")).click();
		
		// verify the deal was created
		Thread.sleep(1500);
		driver.findElement(By.xpath("//strong[text()='Cabana Parrot O� deal']")).click();
		
		// verify the correct due date (future date - deal not overdue yet)
		Thread.sleep(1500);
		String dueDate = driver.findElement(By.cssSelector("div[class='expectedCloseDateWrapper']")).getText();
		Assert.assertEquals(dueDate, "2. veebruar 2020");
		
		// verify the correct deal value (non-default currency)
		String dealValue = driver.findElement(By.cssSelector("div[class='value']")).getText();
		Assert.assertEquals(dealValue, "22 $");
		
		// verify the deal is created with the correct person
		String personName = driver.findElement(By.linkText("Aleksei Suvorov")).getText();
		Assert.assertEquals(personName, defaultPerson);
		
		// verify the company name
		String companyName = driver.findElement(By.linkText("Cabana Parrot O�")).getText();
		Assert.assertEquals(companyName, defaultCompany);
		
	}

}
