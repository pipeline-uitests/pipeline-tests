package testingFramework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;


public class TestHelper {
    
	protected static WebDriver driver;
    
	@BeforeTest
	public void StartTesting() throws InterruptedException {
		
		System.setProperty("webdriver.gecko.driver", "E:\\\\Gecko\\geckodriver.exe");
		driver = new FirefoxDriver();
	
		String loginEmail = "aleksei.suvorov@gmail.com";
		String loginPassword = "CabanaPass";
		String loggedUser = "Aleksei Suvorov";
		String loginUrl = "https://app.pipedrive.com/auth/login";

		driver.get(loginUrl);
		
		Thread.sleep(500);
		driver.findElement(By.id("login")).sendKeys(loginEmail);
		driver.findElement(By.id("password")).sendKeys(loginPassword);
		driver.findElement(By.xpath("//button[text()='Log in']")).click();

		Thread.sleep(2000);
        String foundUser = driver.findElement(By.className("name")).getText();
        Assert.assertEquals(foundUser, loggedUser);
		    
	}
	
	@AfterTest
	public void TestCompleted() {
		driver.close();
	}

}
