Test Task

The project is set up with Selenium with geckodriver and TestNG. The objective was to have
a thin and easily deployable setup freeing up time for writing the actual tests and other
parts of the test task.

The component/flow chosen for automation is adding a new deal in the format of UI tests.
The reason behind this choice is that adding a new deal is one of the most important parts
of the pipeline - it is crucial to be able to add deals with various attributes and for 
them to be added correctly.  

Testing is guided by annotations, the code is commented for clarity. Verification is done
either using asserts or interacting with the component (interaction itself / reading component
attributes tests for presence of the component).

I've tried to keep the amount of tests at a minimum whilst providing testing coverage of 
the added deals by combining items to verify under a single testcase where appropriate.

To run the project: 

1. clone the project from the repository

2. set the geckodriver location in the TestHelper class to where it is located on your system

3. run the project

Geckodriver is available at https://github.com/mozilla/geckodriver/releases 

Best wishes,
Aleksei Suvorov